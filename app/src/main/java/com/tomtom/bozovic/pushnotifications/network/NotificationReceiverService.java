package com.tomtom.bozovic.pushnotifications.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class NotificationReceiverService extends FirebaseMessagingService {
    public static final String FIREBASE_SHARED_PREF_NAME = "firebase-sp";
    public static final String FIREBASE_TOKEN = "firebase-token";
    private static final String TAG = "NotificationReceiverSer";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        PushNotifier.INSTANCE.notifyListeners();
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        final SharedPreferences sharedPreferences = getSharedPreferences(FIREBASE_SHARED_PREF_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(FIREBASE_TOKEN, s).apply();
        Log.d(TAG, "Token = " + s);
    }
}
