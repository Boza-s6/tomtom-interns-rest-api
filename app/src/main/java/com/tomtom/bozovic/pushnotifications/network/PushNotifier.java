package com.tomtom.bozovic.pushnotifications.network;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Dispatches push notifications to registered clients
 */
public class PushNotifier {

    public static final PushNotifier INSTANCE = new PushNotifier();

    public interface PushListener {
        void onPushReceived();
    }
    private final CopyOnWriteArrayList<PushListener> mPushListeners = new CopyOnWriteArrayList<>();

    public PushNotifier() {
    }

    public void addListener(PushListener pushListener) {
        mPushListeners.add(pushListener);
    }

    public void removeListener(PushListener pushListener) {
        mPushListeners.remove(pushListener);
    }

    public void notifyListeners() {
        for (PushListener pushListener : mPushListeners) {
            pushListener.onPushReceived();
        }
    }

}
