package com.tomtom.bozovic.pushnotifications.ui.details;

import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.internal.bind.util.ISO8601Utils;
import com.tomtom.bozovic.pushnotifications.R;
import com.tomtom.bozovic.pushnotifications.datamodels.Todo;

import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Date;

public class TodoDetailsFragment extends Fragment {
    public interface Callback {
        void onTodoNotFound(int notFoundTodoId);
    }
    static class DetailViewModelFactory implements ViewModelProvider.Factory {
        private final int todoId;

        DetailViewModelFactory(int todoId) {
            this.todoId = todoId;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            if (TodoDetailsViewModel.class == modelClass) {
                return (T) new TodoDetailsViewModel(todoId);
            }

            throw new IllegalStateException("Cannot create unknown view model: " + modelClass);
        }
    }

    private static final String ARG_TODO_ID = "details.todoId";
    private TodoDetailsViewModel mViewModel;
    private TextView mName;
    private TextView mDesc;
    private TextView mId;
    private TextView mCreatedAt;
    private TextView mUpdatedAt;

    public static TodoDetailsFragment newInstance(int todoId) {
        Bundle args = new Bundle();
        args.putInt(ARG_TODO_ID, todoId);
        final TodoDetailsFragment todoDetailsFragment = new TodoDetailsFragment();
        todoDetailsFragment.setArguments(args);
        return todoDetailsFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.todo_details_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel = ViewModelProviders.of(this, new DetailViewModelFactory(getTodoId()))
                .get(TodoDetailsViewModel.class);
        mViewModel.getTodo().observe(getViewLifecycleOwner(), this::bindToUi);
        mName = view.findViewById(R.id.name);
        mDesc = view.findViewById(R.id.desc);
        mId = view.findViewById(R.id.id);
        mCreatedAt = view.findViewById(R.id.createdAt);
        mUpdatedAt = view.findViewById(R.id.updatedAt);
    }

    private void bindToUi(@Nullable Todo todo) {
        if (todo == null) {
            goBack();
            return;
        }
        mName.setText(todo.title);
        mDesc.setText(todo.description == null ? "<no description>" : todo.description);
        mId.setText(String.valueOf(todo.id));
        mCreatedAt.setText(getLocalTime(todo.createdAt));
        mUpdatedAt.setText(getLocalTime(todo.updatedAt));
    }

    private String getLocalTime(String createdAt) {
        if (createdAt == null) {
            return "<unknow date>";
        }
        try {
            final Date parserDate = ISO8601Utils.parse(createdAt, new ParsePosition(0));
            return DateFormat.getLongDateFormat(requireContext()).format(parserDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private void goBack() {
        Toast.makeText(requireContext(), "Todo not found " + getTodoId(), Toast.LENGTH_SHORT).show();
        ((Callback) requireActivity()).onTodoNotFound(getTodoId());
    }

    private int getTodoId() {
        return requireArguments().getInt(ARG_TODO_ID);
    }
}
