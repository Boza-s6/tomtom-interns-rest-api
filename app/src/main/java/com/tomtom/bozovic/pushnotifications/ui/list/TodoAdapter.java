package com.tomtom.bozovic.pushnotifications.ui.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tomtom.bozovic.pushnotifications.R;
import com.tomtom.bozovic.pushnotifications.datamodels.Todo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.TodoViewHolder> {
    public interface Callback {
        void onItemClick(int todoId);
    }

    private final LayoutInflater mLayoutInflater;
    private final Callback mCallback;
    private List<Todo> mTodoResponses = Collections.emptyList();


    public TodoAdapter(Context context, Callback callback) {
        mLayoutInflater = LayoutInflater.from(context);
        mCallback = callback;
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public TodoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TodoViewHolder(mLayoutInflater.inflate(R.layout.todo_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TodoViewHolder holder, int position) {
        holder.bind(mTodoResponses.get(position));
    }

    @Override
    public int getItemCount() {
        return mTodoResponses.size();
    }

    @Override
    public long getItemId(int position) {
        return mTodoResponses.get(position).id;
    }

    public void setItems(List<Todo> todoRespons) {
        mTodoResponses = new ArrayList<>(todoRespons);
        notifyDataSetChanged();
    }

    class TodoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView mName;
        private final TextView mId;
        private final TextView mDesc;
        private Todo mTodo;

        public TodoViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mName = itemView.findViewById(R.id.name);
            mId = itemView.findViewById(R.id.id);
            mDesc = itemView.findViewById(R.id.desc);
        }

        public void bind(Todo todo) {
            mTodo = todo;
            mId.setText(String.valueOf(todo.id));
            mName.setText(todo.title);
            mDesc.setText(todo.description != null ? todo.description : "<no description>");
        }

        @Override
        public void onClick(View v) {
            mCallback.onItemClick(mTodo.id);
        }
    }
}
