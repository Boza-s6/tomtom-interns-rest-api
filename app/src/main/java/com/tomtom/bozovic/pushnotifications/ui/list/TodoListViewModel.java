package com.tomtom.bozovic.pushnotifications.ui.list;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.tomtom.bozovic.pushnotifications.network.NetworkModule;
import com.tomtom.bozovic.pushnotifications.network.PushNotifier;
import com.tomtom.bozovic.pushnotifications.datamodels.Todo;
import com.tomtom.bozovic.pushnotifications.network.TodoService;
import com.tomtom.bozovic.pushnotifications.network.TodosResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TodoListViewModel extends ViewModel implements PushNotifier.PushListener {
    private final PushNotifier mPushNotifier;
    private final MutableLiveData<List<Todo>> mListLiveData;
    private final TodoService mTodoService;
    @Nullable
    private Call<TodosResponse> mInProgress = null;

    public TodoListViewModel() {
        mTodoService = NetworkModule.INSTANCE.provideToDoService();
        mPushNotifier = PushNotifier.INSTANCE;
        mListLiveData = new MutableLiveData<>();
        mPushNotifier.addListener(this);
        fetchTodos();
    }

    /**
     * Value holder that holds list of todos
     */
    public LiveData<List<Todo>> getListLiveData() {
        return mListLiveData;
    }

    @Override
    public void onPushReceived() {
        fetchTodos();
    }

    private void fetchTodos() {
        cancelRunningRequest();

        mInProgress = mTodoService.listTodos();
        mInProgress.enqueue(new Callback<TodosResponse>() {
            @Override
            public void onResponse(@NonNull Call<TodosResponse> call,
                                   @NonNull Response<TodosResponse> response) {
                if (response.isSuccessful()) {
                    mListLiveData.setValue(response.body().data);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TodosResponse> call, @NonNull Throwable t) {
                // this should show something in the ui, but this is sample app
            }
        });
    }

    private void cancelRunningRequest() {
        if (mInProgress != null) {
            mInProgress.cancel();
            mInProgress = null;
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        cancelRunningRequest();
        mPushNotifier.removeListener(this);
    }
}
