package com.tomtom.bozovic.pushnotifications.datamodels;

import androidx.annotation.NonNull;

import org.apache.commons.lang3.builder.ToStringBuilder;


public class Todo {
    public Integer id;
    public String title;
    public String description;
    public Boolean isComplete;
    public String createdAt;
    public String updatedAt;

    public Todo withId(Integer id) {
        this.id = id;
        return this;
    }

    public Todo withTitle(String title) {
        this.title = title;
        return this;
    }

    public Todo withDescription(String description) {
        this.description = description;
        return this;
    }

    public Todo withIsComplete(Boolean isComplete) {
        this.isComplete = isComplete;
        return this;
    }

    public Todo withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public Todo withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    @NonNull
    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("title", title)
                .append("description", description)
                .append("isComplete", isComplete)
                .append("createdAt", createdAt)
                .append("updatedAt", updatedAt).toString();
    }

}