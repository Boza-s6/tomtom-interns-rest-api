package com.tomtom.bozovic.pushnotifications.ui.details;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.tomtom.bozovic.pushnotifications.datamodels.Todo;
import com.tomtom.bozovic.pushnotifications.network.NetworkModule;
import com.tomtom.bozovic.pushnotifications.network.PushNotifier;
import com.tomtom.bozovic.pushnotifications.network.TodoService;

public class TodoDetailsViewModel extends ViewModel implements PushNotifier.PushListener {
    private final PushNotifier mPushNotifier;
    private final MutableLiveData<Todo> mTodo;
    private final TodoService mTodoService;
    private final int mTodoId;

    public TodoDetailsViewModel(int todoId) {
        mTodoId = todoId;
        mTodoService = NetworkModule.INSTANCE.provideToDoService();
        mPushNotifier = PushNotifier.INSTANCE;
        mPushNotifier.addListener(this);
        mTodo = new MutableLiveData<>();
        fetchTodo();
    }

    /**
     * Value holder that holds list of todos
     */
    public LiveData<Todo> getTodo() {
        return mTodo;
    }

    @Override
    public void onPushReceived() {
        fetchTodo();
    }

    private void fetchTodo() {
        cancelRunningRequest();
        // todo fetch one todo by id, and update mTodo live data
    }

    private void cancelRunningRequest() {
       // todo cancel runnign request here
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        cancelRunningRequest();
        mPushNotifier.removeListener(this);
    }
}
