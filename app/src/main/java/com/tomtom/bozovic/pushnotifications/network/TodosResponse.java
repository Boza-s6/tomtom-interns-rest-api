package com.tomtom.bozovic.pushnotifications.network;

import com.tomtom.bozovic.pushnotifications.datamodels.Todo;

import java.util.List;

public class TodosResponse {
    public Integer count;
    public Integer page;
    public Integer pageSize;
    public List<Todo> data;
}
