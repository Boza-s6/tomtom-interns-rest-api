package com.tomtom.bozovic.pushnotifications.network;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkModule {
    private TodoService mTodoService;
    private Retrofit mRetrofit;

    public static final NetworkModule INSTANCE = new NetworkModule();

    private NetworkModule() {
        // disallow instantiation
    }

    public synchronized Retrofit provideRetrofit() {
        if (mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .client(provideHttpClient())
                    .baseUrl("https://todo-simple-api.herokuapp.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return mRetrofit;
    }

    private OkHttpClient provideHttpClient() {
        return new OkHttpClient.Builder()
                // Change level to Level.BODY to see request and responses in logcat
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE))
                .build();
    }

    public synchronized TodoService provideToDoService() {
        if (mTodoService == null) {
            mTodoService = provideRetrofit().create(TodoService.class);
        }
        return mTodoService;
    }
}
