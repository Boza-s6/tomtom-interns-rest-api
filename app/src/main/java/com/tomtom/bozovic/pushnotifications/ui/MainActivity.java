package com.tomtom.bozovic.pushnotifications.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.tomtom.bozovic.pushnotifications.R;
import com.tomtom.bozovic.pushnotifications.network.NotificationReceiverService;
import com.tomtom.bozovic.pushnotifications.ui.details.TodoDetailsFragment;
import com.tomtom.bozovic.pushnotifications.ui.list.TodoAdapter;
import com.tomtom.bozovic.pushnotifications.ui.list.TodoListFragment;

public class MainActivity extends AppCompatActivity implements TodoAdapter.Callback,
        TodoDetailsFragment.Callback {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        logtoken();

        if (savedInstanceState == null) {
            // on start show list fragment
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_holder, TodoListFragment.newInstance())
                    .commit();
        }
    }

    private void logtoken() {
        final SharedPreferences sharedPreferences =
                getSharedPreferences(NotificationReceiverService.FIREBASE_SHARED_PREF_NAME, Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString(NotificationReceiverService.FIREBASE_TOKEN, "");
        Log.d(TAG, "token: " + token);
    }

    /** Implements {@link TodoAdapter.Callback} */
    @Override
    public void onItemClick(int todoId) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_holder, TodoDetailsFragment.newInstance(todoId))
                .addToBackStack(null)
                .commit();
    }

    /** Implements {@link TodoDetailsFragment.Callback} */
    @Override
    public void onTodoNotFound(int notFoundTodoId) {
        getSupportFragmentManager().popBackStack();
    }
}

