package com.tomtom.bozovic.pushnotifications.network;

import androidx.annotation.NonNull;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface TodoService {
    @NonNull
    @GET("/todos")
    Call<TodosResponse> listTodos();

    // TODO add mathod for one todo https://square.github.io/retrofit/

}
