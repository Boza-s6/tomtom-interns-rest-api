package com.tomtom.bozovic.pushnotifications.ui.list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tomtom.bozovic.pushnotifications.R;

public class TodoListFragment extends Fragment {
    private TodoListViewModel mViewModel;

    public static TodoListFragment newInstance() {
        return new TodoListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.todo_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View v, @Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(TodoListViewModel.class);

        final RecyclerView recyclerView = v.findViewById(R.id.list);
        final TodoAdapter todoAdapter = initListView(recyclerView);

        //noinspection Convert2MethodRef
        mViewModel.getListLiveData().observe(getViewLifecycleOwner(), todos -> todoAdapter.setItems(todos));
    }

    private TodoAdapter initListView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        final TodoAdapter todoAdapter = new TodoAdapter(requireContext(), (TodoAdapter.Callback) requireActivity());
        recyclerView.setAdapter(todoAdapter);
        return todoAdapter;
    }
}
