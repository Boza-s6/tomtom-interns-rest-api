package com.example.calendarproviderexample;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.calendarproviderexample.EventsViewModel.Event;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventViewHolder> {
    private final LayoutInflater layoutInflater;
    private final Context context;
    private List<Event> mEvents = Collections.emptyList();
    private final java.text.DateFormat timeFormat;

    public EventsAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        timeFormat = DateFormat.getTimeFormat(context);
    }

    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EventViewHolder(layoutInflater.inflate(R.layout.event_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder holder, int position) {
        holder.bind(mEvents.get(position));
    }

    @Override
    public int getItemCount() {
        return mEvents.size();
    }

    public void swapData(List<Event> events) {
        mEvents = new ArrayList<>(events);
        notifyDataSetChanged();
    }

    class EventViewHolder extends RecyclerView.ViewHolder {

        private final TextView title;
        private final TextView desc;
        private final TextView date;

        public EventViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            desc = itemView.findViewById(R.id.desc);
            date = itemView.findViewById(R.id.begin_date);
        }

        public void bind(Event event) {
            title.setText(event.title);
            desc.setText(event.description);
            date.setText(timeFormat.format(event.begins));
        }
    }
}
