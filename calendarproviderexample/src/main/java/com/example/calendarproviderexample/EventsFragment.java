package com.example.calendarproviderexample;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class EventsFragment extends Fragment {
    private static final int READ_CALENDAR_PERMISSION_REQUEST = 1;

    private EventsViewModel mViewModel;

    public static EventsFragment newInstance() {
        return new EventsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.events_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(EventsViewModel.class);

        final EventsAdapter eventsAdapter = setupList();
        mViewModel.getEventsLiveData().observe(getViewLifecycleOwner(), eventsAdapter::swapData);

    }

    @Override
    public void onStart() {
        super.onStart();
        if (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.READ_CALENDAR) ==
                PackageManager.PERMISSION_GRANTED) {
            mViewModel.loadEvents();
        } else {
            requestPermissions(new String[]{Manifest.permission.READ_CALENDAR}, READ_CALENDAR_PERMISSION_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_CALENDAR_PERMISSION_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mViewModel.loadEvents();
            } else {
                Toast.makeText(requireActivity(), "Enable permission to be able to read calendar", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private EventsAdapter setupList() {
        RecyclerView recyclerView = getView().findViewById(R.id.event_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireActivity()));
        EventsAdapter eventsAdapter = new EventsAdapter(requireActivity());
        recyclerView.setAdapter(eventsAdapter);
        return eventsAdapter;
    }

}
