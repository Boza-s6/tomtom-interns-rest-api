package com.example.calendarproviderexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class CalendarEventsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_events);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, EventsFragment.newInstance())
                    .commit();
        }
    }
}
