package com.example.calendarproviderexample;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.CalendarContract.Attendees;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.provider.CalendarContract.Instances;
import android.text.format.Time;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/*Loads events for current day*/
public class EventsViewModel extends AndroidViewModel {

    private static final String[] PROJECTION = new String[]{
            Instances._ID,
            Instances.EVENT_ID,
            Instances.TITLE,
            Instances.DESCRIPTION,
            Instances.BEGIN,
            Instances.END,
            Instances.START_DAY,
    };
    // == 'startDay ASC, begin ASC, title ASC'
    private static final String SORT_ORDER = String.format(Locale.US, "%s ASC, %s ASC, %S ASC", Instances.START_DAY, Instances.BEGIN, Instances.TITLE);
    // non cancelled selection + non declined by user
    private static final String SELECTION;

    static {
        String statusNotCancel = "(" + Events.STATUS + "!=" + Events.STATUS_CANCELED
                + " OR " + Events.STATUS + " IS NULL" + ")";
        SELECTION = Calendars.VISIBLE + "=1 AND " + Instances.SELF_ATTENDEE_STATUS + "!="
                + Attendees.ATTENDEE_STATUS_DECLINED + " AND " + statusNotCancel;
    }

    static class Event {
        long instanceId;
        long eventId;
        String title;
        String description;
        Date begins;
        Date ends;
    }

    @Nullable
    private AsyncTask<?, ?, ?> runningTask;

    private final MutableLiveData<List<Event>> eventsLiveData = new MutableLiveData<>();

    public EventsViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<List<Event>> getEventsLiveData() {
        return eventsLiveData;
    }

    private Uri buildQueryUri() {
        Uri rootUri = Instances.CONTENT_BY_DAY_URI;
        Uri.Builder builder = rootUri.buildUpon();
        ContentUris.appendId(builder, getCurrentJulianDay());
        ContentUris.appendId(builder, getCurrentJulianDay());
        return builder.build();
    }

    private long getCurrentJulianDay() {
        Time selectedDate = new Time(Time.getCurrentTimezone());
        selectedDate.setToNow();
        return Time.getJulianDay(Calendar.getInstance().getTimeInMillis(), selectedDate.gmtoff);
    }

    @SuppressLint("StaticFieldLeak")
    void loadEvents() {
        cancelRunningTasks();
        runningTask = new AsyncTask<Void, Void, List<Event>>() {
            @Override
            protected List<Event> doInBackground(Void... voids) {
                ContentResolver resolver = getApplication().getContentResolver();
                try (Cursor c = resolver.query(buildQueryUri(), PROJECTION, SELECTION, null, SORT_ORDER)) {
                    return mapCursorToEvents(Objects.requireNonNull(c));
                }
            }

            @Override
            protected void onPostExecute(List<Event> events) {
                super.onPostExecute(events);
                eventsLiveData.setValue(events);
            }
        }.execute();
    }

    @WorkerThread
    private List<Event> mapCursorToEvents(Cursor cursor) {
        List<Event> events = new ArrayList<>();
        cursor.moveToPosition(-1);
        while (cursor.moveToNext()) {
            Event event = new Event();
            event.instanceId = cursor.getInt(cursor.getColumnIndexOrThrow(Instances._ID));
            event.eventId = cursor.getLong(cursor.getColumnIndexOrThrow(Instances.EVENT_ID));
            event.title = cursor.getString(cursor.getColumnIndexOrThrow(Instances.TITLE));
            event.description = cursor.getString(cursor.getColumnIndexOrThrow(Instances.DESCRIPTION));
            event.begins = new Date(cursor.getLong(cursor.getColumnIndexOrThrow(Instances.BEGIN)));
            event.ends = new Date(cursor.getLong(cursor.getColumnIndexOrThrow(Instances.END)));
            events.add(event);
        }
        return events;
    }

    private void cancelRunningTasks() {
        if (runningTask != null) {
            runningTask.cancel(true);
            runningTask = null;
        }
    }

}
